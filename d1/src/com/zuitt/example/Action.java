package com.zuitt.example;

public interface Action {
    // Interface - are used to achieve total abstraction.
    public  void sleep();
    public void run();
}
