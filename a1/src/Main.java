import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact();
        contact1.setName("John");
        contact1.setContactNumber("123456789");
        contact1.setAddress("123 Street Brgy 1A New York");
        Contact contact2 = new Contact();
        contact2.setName("Jane");
        contact2.setContactNumber("987654321");
        contact2.setAddress("456 Street Brgy 2B Washington");
        phonebook.setContacts(new ArrayList<String>(Arrays.asList(contact1.getName(), contact1.getContactNumber(), contact1.getAddress())));
        System.out.println(phonebook.getContacts());
        phonebook.setContacts(new ArrayList<String>(Arrays.asList(contact2.getName(), contact2.getContactNumber(), contact2.getAddress())));
        System.out.println(phonebook.getContacts());
    }
}