import java.util.ArrayList;

public class Phonebook extends Contact {
    private ArrayList<String> contacts = new ArrayList<>();
    public Phonebook(){}
    public Phonebook(String name, String contactNumber, String address, ArrayList<String> contacts) {
        super(name, contactNumber, address);
        this.contacts = contacts;
    }
    public void setContacts(ArrayList<String> contacts) {
        this.contacts = contacts;
    }
    public ArrayList<String> getContacts() {
        return contacts;
    }
}
